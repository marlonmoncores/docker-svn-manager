#!/bin/bash

mkdir /home/svn/svnconfig
mkdir /home/svn/repos
touch /home/svn/passwd
touch /home/svn/accessfile
chown www-data.www-data /home/svn/svnconfig
chown www-data.www-data /home/svn/repos
chown www-data.www-data /home/svn/passwd
chown www-data.www-data /home/svn/accessfile


#Creating SVN user
htpasswd -b /home/svn/passwd ${SVN_USER} ${SVN_PASS}

#Running SVN Server
/usr/bin/svnserve -d -r /home/svn/repos

#creating database

while ! mysqladmin ping -h ${DB_HOST} --silent; do
  echo "Waiting for mysql server"
    sleep 3
done
echo "Creating svnmanager database"
mysql -h ${DB_HOST} --password=123 -uroot --execute "CREATE DATABASE ${MANAGER_DATABASE_NAME};"

#Running apache2
/usr/sbin/apache2ctl -D FOREGROUND
