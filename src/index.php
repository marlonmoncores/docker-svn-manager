<html>
<head></head>
<body>
  <h1>SVN Server and Manager</h1>
  Server is up and running. Enjoy!

  <p>Manager: <a href="./manager">open</a></p>
  <p>Repositories: <a href="./svn">open</a></p>

  <hr />
  <p>To checkout a repository use the command: svn checkout http://localhost:8080/svn/{REPO_NAME} --username {USER_NAME} </p>


  <p>Created by: Marlon Monçores: marlon@moncores.com</p>
</body>
</html>
