build:
	docker build -t marlonmoncores/svn_server_and_manager:latest .

run: build;
	docker run -it  marlonmoncores/svn_server_and_manager:latest /bin/bash

compose:
	docker-compose up

compose-d:
	docker-compose up -d

down:
	docker-compose down

exec:
	docker exec -i -t svn-server /bin/bash
