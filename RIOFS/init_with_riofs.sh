#!/bin/bash

echo "Starting RIOFS ... "
riofs -o nonempty,allow_other --uid=33 --gid=33 --dmode=0770 --fmode=0770 -l "/var/log/riofs/riofs.log" -v -c /riofs/config/riofs.conf.xml ${BUCKET_NAME} /home/svn

echo "RIOFS started ..."

sh /usr/init.sh
