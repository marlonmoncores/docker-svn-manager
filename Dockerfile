FROM php:7-apache

RUN apt-get update
RUN apt-get install -y subversion libapache2-mod-svn apache2-dev mysql-client
RUN docker-php-ext-install mysqli && docker-php-ext-enable mysqli
RUN pear install -f -o VersionControl_SVN
RUN a2enmod dav_svn


COPY src/ /var/www/html/
RUN chown -R www-data.www-data /var/www/html/

COPY dav_svn.conf /etc/apache2/mods-available/dav_svn.conf

COPY init.sh /usr/

ENV SVN_USER=${SVN_USER:-user}
ENV SVN_PASS=${SVN_PASS:-user}
ENV MANAGER_DATABASE_NAME=${MANAGER_DATABASE_NAME:-svnmanager}
ENV DB_HOST=${DB_HOST:-localhost}
ENV SMTP_SERVER=${SMTP_SERVER:-undefined}
ENV MANAGER_DATABASE_PASS=${MANAGER_DATABASE_PASS:-123}

# Expose apache.
EXPOSE 80

VOLUME ["/home/svn/", "/var/log/apache2/"]

# By default start up apache in the foreground, override with /bin/bash for interative.
CMD sh /usr/init.sh
